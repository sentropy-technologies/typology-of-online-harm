
![Typology Diagram](images/TypologyOfHarm.png)


This repo reflects the typology discussed in 

M. Banko, B. MacKeen, L. Ray. 2020. _A Unified Typology of Harmful Content_. 
In Proceedings of The 4th Workshop on Online Abuse and Harms.


## Type Definitions

* [Hate and Harassment](src/hate_harassment)
    * [Doxing](src/hate_harassment/doxing)
    * [Identity Attack](src/hate_harassment/identity_attack)
    * [Identity Misrepresentation](src/hate_harassment/identity_misrepresentation)
    * [Insult](src/hate_harassment/insult)
    * [Sexual Aggression](src/hate_harassment/sexual_aggression)
    * [Threat of Violence](src/hate_harassment/threat_of_violence)
* [Self-Inflicted Harm](src/self_inflicted_harm)
    * [Eating Disorder Promotion](src/self_inflicted_harm/eating_disorder_promotion)
    * [Self-Harm](src/self_inflicted_harm/self_harm)
* [Ideological Harm](src/ideological_harm)
    * [Misinformation](src/ideological_harm/misinformation)
    * [Extremism, Terrorism & Organized Crime](src/ideological_harm/extremism_terrorism_org_crime)
        * [White Supremacist Extremism](src/ideological_harm/extremism_terrorism_org_crime/wse)
* [Exploitation](src/exploitation)
    * [Adult Sexual Services](src/exploitation/adult_sexual_services)
    * [Child Sexual Abuse Materials](src/exploitation/csam)
    * [Scams](src/exploitation/scams)