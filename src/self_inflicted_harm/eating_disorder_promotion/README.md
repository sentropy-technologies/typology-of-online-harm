# Eating Disorder Promotion

Eating disorders (EDs) are mental disorders characterized by abnormal eating habits and attitudes towards food. 
Many online platforms explicitly prohibit pro-ED content in order to prevent the spread of unhealthy behavior.

## Examples

* Promotion of eating disorders as legitimate lifestyle choices (e.g. pro-ana, pro-mia content)
* Glorification of slim or emaciated bodies (e.g. thinspiration)
* Content featuring high-fat food or overweight people intended to induce disgust (e.g. reverse thinspiration)
* Sharing instructions for unhealthy weight loss methods

## Non-Examples

* Research, advocacy, and education related to eating disorders
* Discussion of recovery mechanisms and resources to prevent eating disorders
* Anecdotes of individuals who have suffered from eating disorders in a manner that does not glorify the disorder
