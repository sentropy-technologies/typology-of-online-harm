# Self-Harm


_Self-Harm_ is a behavior in which a person purposefully physically hurts themself using methods such as cutting with a sharp object, burning, biting, and pulling out hair. 
Practitioners of such behavior do so in order to cope with emotional distress. 
Suicide is an additional, albeit extremely different, form of self-harm, which we include in this definition. 


## Examples

* Discussion of current or recent acts of deliberately harming one’s own body.
* Suicidal ideation, discussing details of a suicide plan, or stating that one intends to commit suicide
* Requests for instructions on how to conduct or hide self-harm or suicide
* Describing emotions or symptoms of mental illness explicitly related to self-harm, or traumatic experiences and triggers
* Promotion of or assistance with self-harming behaviors 

## Non-Examples

* Anecdotes of personal recovery, treatment
* Sharing coping methods for addressing thoughts of self-harm or suicide
* Support for individuals who are considering or are actively harming themselves
* Recollection of self-harming behaviors or suicidal attempts that occurred at least 12 months in the past that does not promote self-harm or suicide
* Research or education related to prevention of self-harm or suicide
* Discussion of depression or other mental illnesses, symptoms, or depressed thoughts and feelings that are not explicitly tied to self-harm or suicide
