# Identity Attack

_Identity Attack_ is a form of online abuse where malicious actors severely attack individuals or groups of people based on their membership in a protected or vulnerable group. 
During an _Identity Attack_, a bad actor will use language reflecting the intent to dehumanize, persecute or promote violence based on the identity of the subject.
Identity-based attributes considered include age, disability, ethnicity, gender identity, military status, nationality, race, religion, and sexual orientation. 
Additional protections may be offered for vulnerable groups related to immigration status, socio-economic class or the presence of a medical condition. 


## Examples

* Explicit use of slurs and other derogatory epithets referencing an identity group
* Violent threats or calls for harm directed at an identity group
* Calls for exclusion, domination or suppression of rights, directed at an identity group
* Dehumanization of an identity group, including comparisons to animals, insects, diseases or filth, generalizations involving physical unattractiveness, low intelligence, mental instability and/or moral deficiencies
* Expressions of superiority of one group over a protected or vulnerable group
* Admissions of hate and intolerance towards members of an identity group
* Denial of another’s identity, calls for conversion therapy, deadnaming
* Support for hate groups communicating intent described above 

## Non-Examples

* Attacks on institutions or organizations (as opposed to the people belonging to them)
* Promotion of negative stereotypes, fear or misinformation related to an identity group (defined as [Identity Misrepresentation](../identity_misrepresentation)
* In-group usage of slurs and their variants, reclamation of hateful terms by the those who have been historically targeted
* Discussion of meta-linguistic nature or education related to slurs or hate speech
* Accounts of the speech behavior of parties external to the immediate conversational context