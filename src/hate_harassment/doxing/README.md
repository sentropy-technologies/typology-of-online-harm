# Doxing

_Doxing_ is a form of severe abuse in which a malicious party tries to harm an individual by releasing personally identifiable information about the target to the general public. 

## Examples

Personal data that should not be shared without the consent of others includes: 

* Physical or virtual locations such as home, work and IP addresses, or GPS locations
* Contact information such as private email address and phone numbers
* Identification numbers such as Social Security, passport, government or school ids
* Digital identities such as social network accounts, chat identities, and passwords
* Personal financial information such as bank account or credit card information
* Criminal and medical histories

## Non-Examples

Mentions of data already in the public domain such as:

* Place of education or employment
* Email addresses that have been voluntarily shared (such as on a personal homepage)