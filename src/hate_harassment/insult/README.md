# Insult

_Insult_ is described as insulting, inflammatory, or negative comment towards a person or a group of people, in which the target is assumed to be active in the conversation.
Statements that make use of identity-based slurs and epithets are to be elevated to the level of [Identity Attack](../identity_attack). 

## Examples

* General name-calling, directed profanity and other insulting language or imagery not referencing membership in a protected group or otherwise meeting the criteria for [Identity Attack](../identity_attack)
* Content mocking someone for their personality, opinions, character or emotional state
* Body shaming, attacks on physical appearance, or shaming related to sexual or romantic history
* Mocking someone due to their status as a survivor of assault or abuse
* Encouraging others to insult an individual
* Images manipulated with the intent to insult the subject

## Non-Examples
* Insults strictly based on the target’s membership in a group with protected status, e.g. [Identity Attack](../identity_attack)
* Insults aimed at non-participant subjects, such as celebrities and other high-profile individuals
* Self-referential insults and self-deprecation
* Insults directed at inanimate objects
* Harassment education or awareness