# Threat of Violence


_Threat of Violence_ describes state a desire to kill or inflict physical harm towards others. 
Statements which celebrate, encourage or condone violent acts are also included


## Examples

* Desire to physically harm a person or group of people, including violent sexual acts
* Call for the death, serious injury or illness of a person or group of people
* Encouragement of another individual to commit self-harm or suicide
* Incitement to commit acts of violence
* Glorification of violence or violent events

## Non-Examples

* Anecdotal or personal accounts of violence without glorification (e.g. survivor stories, criminal rehabilitation accounts) 
* Historical descriptions or research studies of violence
* Hyperbolic or metaphorical violence

