# Sexual Aggression

_Sexual Aggression_ includes unwanted sexual advances, undesirable sexualization, non-consensual sharing of sexual content, and other forms of unsolicited sexual conversations.

## Examples
* Threats or descriptions of sexual activity, fantasy or non-consensual sex acts directed at an individual
* Unsolicited graphic descriptions of a person (including oneself) that are sexual in nature
* Unwanted sexualization, sexual advances or comments intended to sexually degrade an individual
* Solicitations or offers of non-commercial sexual interactions
* Unwanted requests for nude or sexually graphic images or videos
* Sharing of content depicting any person in a state of nudity or engaged in sexual activity created or shared without their permission, including fakes (e.g. revenge porn)
* Sharing of content revealing intimate parts of a person’s body, even if clothed or in public, created or posted without their permission (e.g., “creepshots” or “upskirt” images)
* Sextortion, threat of exposing a person's intimate images, conversations or other intimate information

## Non-Examples
* Pornography created with consent of all participants
* Solicitation or offers of commercial sex transactions
* Definitions of sexual terms
* Sexual health and wellness discussions
* Non-graphic use of words associated with sex
* Insults that make use of sexual terms 
* Flirting, compliments, or come-ons that are not sexually graphic or degrading


