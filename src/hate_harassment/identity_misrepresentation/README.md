# Identity Misrepresentation

_Identity Misrepresentation_ is defined by statements or claims that are used to convey pejorative misrepresentations, stereotypes, and other insulting generalizations about protected or vulnerable populations. 
Protected groups are defined by attributes including age, disability, ethnicity, gender identity, military status, nationality, race, religion, and sexual orientation. 
Vulnerable groups such as those defined by immigration status, socio-economic class or the presence of a medical condition, may also be offered protection.

## Examples

* Dissemination of negative stereotypes and generalizations about a protected or vulnerable group, apart from those that involve explicit dehumanization or claims of inferiority
* Statements about protected or vulnerable groups presented as declarative truth without supporting evidence
* Microaggressions, subtle expressions of bias towards a protected or vulnerable group
* Intent to spread fear of protected or vulnerable groups, without calls for violence


## Non-Examples

Positive criteria defined for [Identity Attack](../identity_attack) and [Insult](../insult) should be considered non-examples of _Identity Misrepresentation_.

