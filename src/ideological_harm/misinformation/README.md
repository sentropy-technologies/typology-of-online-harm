# Misinformation

_Misinformation_ is false or misleading information. 
It may be spread by users who are unaware of its credibility and lack a deliberate intent to harm. 
_Disinformation_, a subset of misinformation, refers to the knowing spread of misinformation. 
The intent behind disinformation is malicious, such as to damage the credibility of a person or organization, or to gain political or financial advantage. 

## Examples
* Medically unproven health claims that create risk to public health and safety, including the promotion of false cures, incorrect information about public health or emergencies
* False or misleading content about members of protected or vulnerable groups
* False or misleading content that compromises the integrity of an election, or civic participation in an election
* Conspiracy theories
* Denial of a well-documented event
* Opinion spam, fabricated product reviews
* Removal of factual information with intent to erode trust or inflict harm, such as the omission of date, time or context
* Manipulation of visual or audio content with the intent to deceive