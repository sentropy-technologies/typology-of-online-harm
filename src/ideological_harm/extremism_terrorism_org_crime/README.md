# Extremism, Terrorism and Organized Crime

The UN General Assembly defines terrorism as “criminal acts intended or calculated to provoke a state of terror in the public, a group of persons or particular persons for political purposes are in any circumstance unjustifiable, whatever the considerations of a political, philosophical, ideological, racial, ethnic, religious or any other nature that may be invoked to justify them.” 
While terrorist groups are predominantly associated with violent behaviors, extremism refers to both violent and peaceful forms of expression. 
Organized crime groups, which frequently engage in violent criminal behavior, are not typically driven by political or ideological goals, but instead operate for economic gain.

## Examples
* Recruiting for a terrorist organization, extremist group or organized crime group
* Praise and promotion of organized crime, terrorist or extremist groups, or acts committed by such groups
* Assisting a terrorist organization, extremist group or organized crime group
* Content that includes symbols known to represent a terrorist organization, extremist group or organized crime group




