# White Supremacist Extremism

_White Supremacist Extremism_ (_WSE_) describes content seeking to revive and implement various ideologies of white supremacy. 

## Examples

* Neo-Nazism: idolization of Adolph Hitler, praise of Nazi policies or beliefs, use of Nazi symbols or slogans
* White racial supremacy: belief in white racial superiority, promotion of eugenics, incitement or allusions to a race war, concerns about “white genocide,” cynicism towards interracial relationships and miscegenation
* White cultural supremacy: promotion of a white ethnostate, xenophobic attitudes, nostalgia for times of segregation
* Holocaust denial, propagation of Jewish conspiracy theories
* Recruitment or requests for financial support for WSE ideology, incitement of extreme physical fitness as a readiness measure for race-driven conflict